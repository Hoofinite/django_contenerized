#!/bin/bash

set -e

# echo "Collecting data for running migrations"
# ECS_GROUP_ID=$(aws ec2 describe-security-groups --filters Name=group-name,Values=prod-ecs-backend --query "SecurityGroups[*][GroupId]" --output text)
# PRIVATE_SUBNET_ID=$(aws ec2 describe-subnets  --filters "Name=tag:Name,Values=prod-private-1" --query "Subnets[*][SubnetId]"  --output text)

echo "Running migration task..."
#NETWORK_CONFIGURATON="{\"awsvpcConfiguration\": {\"subnets\": [\"${PRIVATE_SUBNET_ID}\"], \"securityGroups\": [\"${ECS_GROUP_ID}\"],\"assignPublicIp\": \"DISABLED\"}}"
# migration task
# echo "Network configuration created"
#MIGRATION_TASK_ARN=$(aws ecs run-task --cluster prod --task-definition backend-migration --count 1 --launch-type FARGATE --network-configuration "${NETWORK_CONFIGURATON}"  --output text)
# echo "Task ${MIGRATION_TASK_ARN} running..."
# wait for migration task to complete
#aws ecs wait tasks-stopped --cluster prod --tasks "${MIGRATION_TASK_ARN}"
echo "Updating web..."
# Updating web service
#aws ecs update-service --cluster prod --service prod-backend-web --force-new-deployment  --query "service.serviceName"  --output json
aws ecs update-service --cluster prod --service prod-backend-web --force-new-deployment --region eu-west-3
echo "Done!"
