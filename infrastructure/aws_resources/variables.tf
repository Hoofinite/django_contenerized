variable "region" {
  description = "The AWS region to create resources in"
  default     = "eu-west-3"
}

variable "project_name" {
  description = "Project name to use in resource names"
  default     = "django-contenerized"
}


variable "availability_zone" {
  description = "Availability zones"
  default     = ["eu-west-3a", "eu-west-3b"]
}

variable "ecs_cloudwatch_retention_days" {
  description = "Days number for log retention"
  default     = 30
}

# route 53 variables
variable "domain_name" {
  default     = "findmyweather.click"
  description = "Domain name registered via Route53"
  type        = string
}

variable "record_name" {
  default     = "www"
  description = "Sub domain name"
  type        = string
}
