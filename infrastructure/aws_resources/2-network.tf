resource "aws_vpc" "prod" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
}

resource "aws_vpc" "test" {
  cidr_block = "192.168.0.0/24"
  enable_dns_hostnames = false
  enable_dns_support = false
}


resource "aws_subnet" "prod_public1" {
    cidr_block = "10.0.1.0/24"
    vpc_id = aws_vpc.prod.id
    availability_zone = var.availability_zone[0]
    tags = {
        Name = "prod-public-1"
    }
}

resource "aws_subnet" "prod_public2" {
    cidr_block = "10.0.2.0/24"
    vpc_id = aws_vpc.prod.id
    availability_zone = var.availability_zone[1]
    tags = {
        Name = "prod-public-2"
    }
}

resource "aws_subnet" "prod_private_1" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.prod.id
  availability_zone = var.availability_zone[0]
    tags = {
        Name = "prod-private-1"
    }
}

resource "aws_subnet" "prod_private_2" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.prod.id
  availability_zone = var.availability_zone[1]
    tags = {
        Name = "prod-private-2"
    }
}

resource "aws_route_table" "prod_public" {
  vpc_id = aws_vpc.prod.id
}

resource "aws_route_table_association" "prod_public1" {
  route_table_id = aws_route_table.prod_public.id
  subnet_id = aws_subnet.prod_public1.id
}

resource "aws_route_table_association" "prod_public2" {
  route_table_id = aws_route_table.prod_public.id
  subnet_id = aws_subnet.prod_public2.id
}

resource "aws_route_table" "prod_private" {
  vpc_id = aws_vpc.prod.id
}

resource "aws_route_table_association" "prod_private1" {
  route_table_id = aws_route_table.prod_private.id
  subnet_id = aws_subnet.prod_private_1.id
}

resource "aws_route_table_association" "prod_private2" {
  route_table_id = aws_route_table.prod_private.id
  subnet_id = aws_subnet.prod_private_2.id
}


resource "aws_internet_gateway" "prod_igw" {
  vpc_id = aws_vpc.prod.id
}
resource "aws_route" "prod_igw_route" {
  route_table_id = aws_route_table.prod_public.id
  gateway_id = aws_internet_gateway.prod_igw.id
  destination_cidr_block = "0.0.0.0/0"
}


resource "aws_eip" "prod_nate_gatew" {
  vpc = true
  associate_with_private_ip = "10.0.0.5"
  depends_on = [
    aws_internet_gateway.prod_igw
  ]
}

resource "aws_nat_gateway" "prod" {
  allocation_id = aws_eip.prod_nate_gatew.id
  subnet_id = aws_subnet.prod_public1.id
}
resource "aws_route" "prod_nate_gateway" {
  route_table_id = aws_route_table.prod_private.id
  nat_gateway_id = aws_nat_gateway.prod.id
  destination_cidr_block = "0.0.0.0/0"
}

