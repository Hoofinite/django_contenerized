resource "aws_lb" "prod_lb" {
  name = "application-LB"
  load_balancer_type = "application"
  internal = false
  security_groups = [aws_security_group.prod_lb.id]
  subnets = [aws_subnet.prod_public1.id, aws_subnet.prod_public2.id]
}

resource "aws_lb_target_group" "prod_backend" {
  name = "prod-backend"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.prod.id
  target_type = "ip"
  health_check {
    path        = "/"
    port        = "traffic-port"
    healthy_threshold = 5
    unhealthy_threshold = 2
    timeout = 2
    interval = 5
    matcher = "200"
  } 
}

resource "aws_lb_listener" "prod_http" {
  load_balancer_arn = aws_lb.prod_lb.arn
  port = "80"
  protocol = "HTTP"
  depends_on = [
    aws_lb_target_group.prod_backend
  ]
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.prod_backend.arn
  }

}

resource "aws_security_group" "prod_lb" {
  name = "prod-lb"
  description = "Controls access to the ALB"
  vpc_id = aws_vpc.prod.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


}