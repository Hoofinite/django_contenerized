provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket         = "my-terraform-state-bucket-p49fdz8a"
    key            = "state/terraform.tfstate"
    region         = "eu-west-3"
    dynamodb_table = "prod_dynamodb_table"
    kms_key_id     = "alias/terraform-bucket-key"
    encrypt        = true
  }
  required_providers {
    aws = {
      version = "4.28"
      source  = "hashicorp/aws"
    }
  }
}

