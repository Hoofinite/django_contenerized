# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket
# Provides a S3 backend bucket resource for managing terraform state file
resource "aws_s3_bucket" "terraform_state_bct" {
  bucket = "my-terraform-state-bucket-p49fdz8a"
  
  tags = {
    Name        = "My bucket"
    Environment = "Prod"
  }
  
}
