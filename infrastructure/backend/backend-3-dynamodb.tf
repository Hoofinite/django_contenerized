# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table
# provides a DynamoDB table resource for terraform state lock
resource "aws_dynamodb_table" "prod_dynamodb_table" {
  name = "prod_dynamodb_table"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }


}