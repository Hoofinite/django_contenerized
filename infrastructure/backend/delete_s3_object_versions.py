#!/usr/bin/env python

import boto3


BUCKET = 'my-terraform-state-bucket-p49fdz8a'

s3 = boto3.resource('s3')
bucket = s3.Bucket(BUCKET)
bucket.object_versions.delete()

#in case if removal of the s3 bucket by script would be a necessity, just uncomment following line:
#bucket.delete()
