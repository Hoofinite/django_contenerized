# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key
# Manages a single-Region or multi-Region primary KMS key.

# Configure the AWS Provider
provider "aws" {}


resource "aws_kms_key" "terraform-bucket-key" {
  description = "KMS key to S3 bucket"
  deletion_window_in_days = 10
  enable_key_rotation = true
}

resource "aws_kms_alias" "mykey_alias" {
  name = "alias/terraform-bucket-key"
  target_key_id = aws_kms_key.terraform-bucket-key.id
}