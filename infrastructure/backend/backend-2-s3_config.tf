
# S3 Buckets only support a single lifecycle configuration. 
# https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-lifecycle-mgmt.html
/* resource "aws_s3_bucket_lifecycle_configuration" "example_config_s3" {
  bucket = aws_s3_bucket.terraform_state_bct.id

  rule {
    id = "log"

    expiration {
      days = 30
    }

    filter {
      and {
        prefix = "log/"

        tags = {
          rule      = "log"
          autoclean = "true"
        }
      }
    }

    status = "Enabled"

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }
  }

} */


resource "aws_s3_bucket_acl" "acl_s3bucket" {
  bucket = aws_s3_bucket.terraform_state_bct.id
  acl    = "private"
}

# enable versioning on the S3 bucket so every update to a file in bucket create a new one version 
# https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/resources/s3_bucket_versioning
resource "aws_s3_bucket_versioning" "vcs_s3bucket" {
  bucket = aws_s3_bucket.terraform_state_bct.id
  versioning_configuration {
    status = "Enabled"
  }
}

# turn the server-side encryption on by default for all data written to this s3 bucket
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration
resource "aws_s3_bucket_server_side_encryption_configuration" "sse_config" {
  bucket = aws_s3_bucket.terraform_state_bct.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.terraform-bucket-key.arn
      sse_algorithm = "aws:kms"
    }
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block
# Manages S3 bucket-level Public Access Block configuration
resource "aws_s3_bucket_public_access_block" "public_access_deny" {
  bucket = aws_s3_bucket.terraform_state_bct.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
