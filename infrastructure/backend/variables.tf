variable "region" {
  description = "The AWS region to create resources in"
  default     = "eu-west-3"
}

variable "project_name" {
  description = "Project name to use in resource names"
  default     = "django-contenerized"
}


