from django.test import TestCase
from django.db import connection

# Create your tests here.
class DbConnectionTests(TestCase):
    def test_database_connection(self):
        self.assertTrue(connection.is_usable())
        
