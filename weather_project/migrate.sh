#!/bin/ash
cd /app/

/opt/.venv/bin/python manage.py makemigrations
/opt/.venv/bin/python manage.py migrate --noinput
/opt/.venv/bin/python manage.py createsuperuser --noinput

echo "migrations && collectstatic done"
