from urllib.request import Request
from django.shortcuts import render
import requests
import datetime
import os

def index(request):
    
    if 'city' not in request.POST:
        city = 'London'
    else:
        if len(request.POST['city']) > 25:
            city = 'London'
        else:
            city = request.POST['city']
            

    URL = 'https://api.openweathermap.org/data/2.5/weather'
    PARAMS = {'q': city, 'appid': os.getenv("SECRET_KEY_WEATHER_API"), 'units': 'metric'}
    day = datetime.date.today()
    try:
        req = requests.get(url=URL, params=PARAMS)
        resp = req.json()
        description = resp['weather'][0]['description']
        icon = resp['weather'][0]['icon']
        temp = resp['main']['temp']
        feel_like = resp['main']['feels_like']
        press = resp['main']['pressure']
        humid = resp['main']['humidity']
        wind_sp = resp['wind']['speed']
        cloud = resp['clouds']['all']
        return render(request, 'weatherapp/index.html', {'description': description, 'icon': icon, 'temp': temp, 'feel_like':feel_like , 'day': day, 'city': city, 'pressure': press, 'humidity': humid, 'wind_speed': wind_sp, 'clouds': cloud})

    except (KeyError, ValueError, Exception) as e:
        description = 'No weather info available for '+city
        icon = 'static/not_found.png'
        return render(request, 'weatherapp/index.html', {'description': description, 'icon': icon, 'temp': None, 'day': day, 'city': city})
