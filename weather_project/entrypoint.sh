#!/bin/ash
APP_PORT=${PORT:-8000}
cd /app/
source /app/migrate.sh

/opt/.venv/bin/gunicorn --worker-tmp-dir /dev/shm weather_project.wsgi:application -b "0.0.0.0:${APP_PORT}"
