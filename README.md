# **[Django contenerized](https://gitlab.com/Hoofinite/django_contenerized)**

The role of this repository is for expanding my skills in Python web development and deployment using Kubernetes & AWS.

# Tech stack
* Python 3.9
* Django 4.1
* OpenWeatherMap.org for Rest API calls
* Bootstrap 5.2.0
* Terraform 4.27 (AWS launch_type = FARGATE)
* Gitlab CI/CD


# Usage documentation to run django application locally:
Clone project by simply pasting to your terminal / command line:
`git clone https://gitlab.com/Hoofinite/django_contenerized `

Create file called **.env** inside /weather_project/weather_project/ and paste in output of the command:\
`python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'`\
`SECRET_KEY=your_generated_new_secret_key`

then go to https://openweathermap.org/ and obtain an API Key\
after this operation append your **.env** file with following format:

`SECRET_KEY='*your_secret_key_django'`\
`SECRET_KEY_WEATHER_API='your_secret_openweather_api_key'`

after that:

`cd weather_project `\
`python manage.py runserver`

Then simply type in your web browser following link:\
`0.0.0.0:8000`
