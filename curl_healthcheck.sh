while true; do
    STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}" http://www.findmyweather.click/)
    if [ $STATUS_CODE -eq 200 ]; then
        echo "Website is up, status code is $STATUS_CODE"
    else
        echo "Website is down, status code is $STATUS_CODE"
    fi
    sleep 1
done
